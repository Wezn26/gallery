<?php
require_once __DIR__ . '/Controllers/functions/autoload.php';
use \App\Models\Gallery\User;

$user = new User;

if (empty($_POST['login']) && $_POST['password']) {
  header('Location: form.php');
  exit;
}

$login = $_POST['login'];
$password = $_POST['password'];

/*
1. ЕСЛИ пользователь уже вошел , ТО редирект на главную страницу
2. ЕСЛИ пользователь не вошел - отображает форму входа
3. ЕСЛИ введены данные в форму входа - проверяем их и ЕСЛИ проверка прошла, ТО запоминаем информацию о вошедшем пользователе
 */
if (!$user->checkPassword($login, $password)) {
  header('Location: ./Controllers/templates/form.php');
  exit;
}

$user->login($login);
$user->getCurrentUser();
header('Location: index.php');
exit;
