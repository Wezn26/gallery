<?php
require_once __DIR__ . '/Controllers/functions/autoload.php';
use \App\Models\Gallery\DB;
use \App\Models\Gallery\Upload;
use \App\Models\Gallery\User;

if (!empty($_POST['image']) && !empty($_FILES['file'])) {  
  $user = new User;
  $upload = new Upload;  
  $date = date('d.m.Y');
  $name = $user->getCurrentUser();
  $image = strip_tags($_POST['image']);
  $res = $upload->fileUpload('file');
  if (false != $res) {
    $path = (string)$res;
  } else {
    die('ERORR $res!!!');
  }
  $db = new DB;
  $sql = 'INSERT INTO gallery
         (date, name, image, path)
         VALUES
         (:date, :name, :image, :path)';
  $data = [
    ':date' => $date,
    ':name' => $name,
    ':image' => $image,
    ':path' => $path
  ];
  
  $line = $db->query($sql, $data);
  
  if (false === $line) {
    die('Изображение не загружено!!!');
  } else {
    header('Location: index.php');
    exit;
  }
} else {
  die('Данные не отправлены на сервер!!!');
}
