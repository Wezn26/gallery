<?php
namespace App\View\Gallery; 
class View
{
  protected $data = [];
  public function assign($name, $value)
  {
    $this->data[$name] = $value;
  }
  public function display($template)
  {
    include __DIR__ . '/../../../../Controllers/templates/' . $template;
  }
}