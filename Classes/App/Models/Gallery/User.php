<?php 
namespace App\Models\Gallery;
class User
{  
  protected $users = [
    'Rita' => 'qwerty',
    'Bria' => '123456'
  ];
   public function getUsersList()//Возвращает массив всех пользователей и хешей их паролей
  {  
    $usersList = [];
    foreach ($this->users as $login => $password) {
      $usersList[$login] = password_hash($password, PASSWORD_DEFAULT);
    }
    return $usersList;
  }
   public function existsUser($login)//Проверяет существует ли пользователь с заданным логином?
  {
    $user = new User;
    $newuser = $user->getUsersList();
    return isset($newuser[$login]);
  }
  public function checkPassword($login, $password)//Возвращает true тогда, когда существует пользователь с указанным логином и введёныый им пвроль прошёл проверку
  {
    $user = new User;
    $newuser = $user->getUsersList();
    return isset($newuser[$login]) && password_verify($password, $newuser[$login]);
  }
  public function login($login)//Устанавливаем куку
  {
    setcookie('user', $login, time()+3600);
  }

  public function getCurrentUser()// Возвращает либо имя вошедшего на сайт, либо null
  {
    if (isset($_COOKIE['user'])) {
      return $_COOKIE['user'];
    } else {
      return null;
    }
  }
  public function logout()//Выход с сайта
  {
    unset($_COOKIE['user']);
    setcookie('user', '', time()-3600);
  }

}

$user = new User;
//var_dump($user->getUsersList());
//var_dump($user->existsUser('Bria'));
//assert(true === $user->existsUser('Rita'));
//var_dump($user->checkPassword('Rita', 'qwerty'));
//assert(true === $user->checkPassword('Bria', '123456'));
//$user->login('Rita');
//var_dump($user->getCurrentUser());

