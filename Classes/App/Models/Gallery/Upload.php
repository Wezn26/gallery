<?php
namespace App\Models\Gallery;
class Upload
{
  public function checkTypeFile($mimeType)
  {
    $mediaTypes = ['image/gif', 'image/svg+xml', 'image/jpeg', 'image/png'];
    return in_array($mimeType, $mediaTypes);
  }

  public function fileUpload($field)
  {
    $file = $_FILES[$field];
    $upload = new Upload;
    if (empty($file) && 0!= $file['error'] && !$upload->checkTypeFile($file['type'])) {
      return false;
    }

    if (is_uploaded_file($file['tmp_name'])) {
       $res = move_uploaded_file($file['tmp_name'], 
      __DIR__ . '/../../../../Controllers/images/' . $file['name']);
      if ($res) {
        return '/images/' . $file['name'];
      } else {
        return false;
      }
    }
    return false;
  }

  public function getPicture()
  {
    $scanDir = scandir(__DIR__. './../../../../Controllers/images/');
    return array_diff($scanDir, ['.', '..']);
  }


}
