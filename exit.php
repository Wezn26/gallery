<?php
require_once __DIR__ . '/Controllers/functions/autoload.php';
use \App\Models\Gallery\User;
$user = new User;

$user->logout();
header('Location: ./Controllers/templates/form.php');
exit;
