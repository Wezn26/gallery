<?php
require_once __DIR__ . '/../functions/autoload.php';

use \App\Models\Gallery\DB;
use \App\Models\Gallery\Upload;
use \App\Models\Gallery\User;

$db = new DB;
$upload = new Upload;
$user = new User;

if (!$user->getCurrentUser()) {
  header('Location: ./Controllers/templates/form.php');
}
?>
<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>GALLERY</title>
</head>

<body>
  <h1>Добро пожаловать <?php echo $user->getCurrentUser(); ?>!!!</h1>
  <a href="exit.php" style="cursor: pointer;">Выход с сайта</a>

  <form action="send.php" method="post" enctype="multipart/form-data">
    <fieldset>
      <legend>Загрузка файла</legend>
      <p>
        <p>Название картинки</p><br>
        <input type="text" name="image" placeholder="Введите название картинки">
      </p>
      <p>
        <p>Загрузка файла:</p><br>
        <input type="file" name="file">
      </p>
      <button type="submit">Отправить на сервер</button>
    </fieldset>
  </form>
  <h2>История</h2>
  <table border="1">
    <tr>
      <th>Дата</th>
      <th>Пользователь</th>
      <th>Подпись</th>
      <th>Картинка</th>
    </tr>
    <?php foreach ($this->data['gallery'] as $record) : ?>
      <tr>
        <td><?php echo $record[1]; ?></td>
        <td><?php echo $record[2]; ?></td>
        <td><?php echo $record[3]; ?></td>
        <td><img src="/GALLERY/Controllers/<?php echo $record[4]; ?>" width="100px"></td>
      </tr>
    <?php endforeach; ?>
  </table>

</body>

</html>