<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Авторизация</title>
</head>
<body>
  <h1>Галлерея</h1>
  <div style="width: 200px;">
    <form action="./../../login.php" method="post">
      <fieldset>
        <legend>Вход на сайт</legend>
        <p>
          <b>Логин:</b><br>
          <input type="text" name="login">
        </p>
        <p>
          <b>Пароль:</b><br>
          <input type="password" name="password">
        </p>
        <button type="submit">Войти</button>
      </fieldset>
    </form>
  </div>
</body>
</html>
