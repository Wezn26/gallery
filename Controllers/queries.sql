CREATE TABLE gallery(
  id SERIAL,
  date VARCHAR(100),
  name VARCHAR(100),
  image VARCHAR(100),
  path TEXT
);
